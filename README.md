# Simulation of Urban Logistic For Reinforcement

+ Built upon Simulation of Urban MObility (SUMO) <sup>[1](#sumo)</sup>
+ Stochastic Vehicle Routing Problem with Time Windows <sup>[2](#vrp)</sup>
+ Modeled as Decentralized Semi-Markov Decision Process (Dec-SMDP) <sup>[3](#decsmdp)</sup>

<a name="sumo"></a>
<a name="vrp"></a>
<a name="decsdmp"></a>

TODO: Better interface, only expose vectors of floats for state, integers for options index, etc...
