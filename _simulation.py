import sulfr
from collections import namedtuple

HistoryStep = namedtuple("HistoryStep",[
    "timestamp",
    "cumul_reward",
    "network_state",
    "orders_state",
    "terminated",
    "agents_states",
    "options"
    ])

class Simulation:
    def __init__(self, simID, scenario, policy):
        """
        Constructor.
        Args:
            - simID    [str]            :
            - scenario [sulfr.Scenario] :
        """
        self.id = simID
        self.scenario = scenario
        self.order_sched = sulfr.OrderScheduler(self)
        self.agents = [sulfr.Agent("{}_veh_{}".format(vtype_ID, i), vtype_ID, self)
            for vtype_ID, number in self.scenario.fleet.items() for i in range(number)]
        self.n_agents = len(self.agents)
        self.agentIndices = {ag.id:i for i,ag in enumerate(self.agents)}
        self.policy = policy

    def reset(self):
        sulfr.traci.start(["sumo"] + self.scenario.getCommandLineArgs(), label = self.id)
        ### DEBUG
        # sulfr.traci.start(["sumo-gui", "-S", "-Q"] + self.scenario.getCommandLineArgs(), label = self.id)
        ###
        self.co = sulfr.traci.getConnection(self.id)
        self.order_sched.reset()
        for ag in self.agents:
            ag.reset()
        self.history = []
        self.r_cumul = 0
        self.last_epoch = 0

    def step(self, t = 0):
        self.co.simulationStep()
        self.order_sched.step(t)
        r = 0
        for ag in self.agents:
            r += ag.step(t)
        self.r_cumul += self.scenario.r_params.discount**(t-self.last_epoch) * r

    def check_termination(self, t = 0):
        terminated = [ag.option.is_terminated() for ag in self.agents]
        if any(terminated):
            ag_states = []
            options = []
            opt_idx = []
            for ag in self.agents:
                if ag.option.is_terminated():
                    ag.option = self.policy.getOption(ag, self)
                    ag_states.append(ag.getState())
                    options.append(ag.option)
                    opt_idx.append(ag.optionIndices[ag.option.id])
                    ag.option.reset()
                else:
                    ag_states.append(None)
                    options.append(None)
                    opt_idx.append(None)

            self.history.append( HistoryStep(t, self.r_cumul,
                self.getNetworkState(), self.getOrdersState(),
                terminated, ag_states, opt_idx) )
            self.r_cumul = 0
            self.last_epoch = t

            invalid = [not opt.can_start(t) for opt in options if opt is not None]
            if any(invalid):
                self.history.append( HistoryStep(t+1, -self.scenario.r_params.invalid_opt_cost,
                    [], [], [True for ag in self.agents], [], []) )
                return False
        return True

    def run(self):
        self.reset()
        for t in range(self.scenario.horizon):
            self.step(t)
            if not self.check_termination(t):
                break
        else:
            self.history.append( HistoryStep(t, self.r_cumul,
                [], [], [True for ag in self.agents], [], []) )
        self.co.close()
        return self.history

    def getPendingOrders(self):
        return self.order_sched.pending

    def getOrdersState(self):
        return self.order_sched.getState()

    def getNetworkState(self):
        return [2*self.co.edge.getTraveltime(e.getID()) / self.scenario.horizon - 1 for e in self.scenario.net.getEdges()]
