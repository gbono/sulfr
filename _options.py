class PickupOption:
    def __init__(self, agent, order):
        self.id = "PICKUP_{}_{}".format(agent.id, order.id)
        self.agent = agent
        self.order = order
        self.reward = self.agent.sim.scenario.r_params.pick_reward

    def __str__(self):
        return "{} picks up {} at {} ({}).".format(self.agent.id, self.order.id, self.order.source.id, self.order.source.edgeID)

    def can_start(self, t = 0):
        return not self.order.hidden \
                and self.order.carrier is None \
                and self.agent.payload + self.order.weight < self.agent.max_payload

    def reset(self):
        self.agent.move_to(self.order.source)
        self.start_load = -1001

    def step(self, t = 0):
        if self.agent.parked \
                and self.agent.is_near(self.order.source) \
                and t >= self.order.ready:
                    if self.start_load < 0:
                        self.start_load = t
                    elif t >= self.start_load + self.order.duration:
                        self.agent.load(self.order)
                        return self.reward
        return 0

    def is_terminated(self):
        return self.order.carrier is not None


class DeliverOption:
    def __init__(self, agent, order):
        self.id = "DELIVER_{}_{}".format(agent.id, order.id)
        self.agent = agent
        self.order = order
        self.reward = self.agent.sim.scenario.r_params.delv_reward
        self.fee = self.agent.sim.scenario.r_params.late_fee

    def __str__(self):
        return "{} delivers {} at {} ({}).".format(self.agent.id, self.order.id, self.order.dest.id, self.order.dest.edgeID)

    def can_start(self, t = 0):
        return not self.order.delivered \
                and self.order.carrier == self.agent

    def reset(self):
        self.agent.move_to(self.order.dest)
        self.start_load = -1001

    def step(self, t = 0):
        if self.agent.parked \
                and self.agent.is_near(self.order.dest) \
                and t >= self.order.early:
                    if self.start_load < 0:
                        self.start_load = t
                    elif t >= self.start_load + self.order.duration:
                        self.agent.unload(self.order)
                        return self.reward - self.fee * max(0, t - self.order.late)
        return 0

    def is_terminated(self):
        return self.order.delivered


class RechargeOption:
    def __init__(self, agent, station):
        self.id = "CHARGE_{}_{}".format(agent.id, station.id)
        self.agent = agent
        self.station = station

    def __str__(self):
        return "{} recharges at {} ({}).".format(self.agent.id, self.station.id, self.station.edgeID)

    def can_start(self, t = 0):
        return True

    def reset(self):
        self.agent.move_to(self.station)

    def step(self, t = 0):
        return 0

    def is_terminated(self):
        return self.agent.charge == self.agent.max_charge


class IdleOption:
    def __init__(self, agent):
        self.id = "IDLE_{}".format(agent.id)
        self.agent = agent

    def __str__(self):
        return "{} stays idle.".format(self.agent.id)

    def can_start(self, t = 0):
        return True

    def reset(self):
        pass

    def step(self, t = 0):
        return 0

    def is_terminated(self):
        return True
