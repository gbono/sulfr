import sulfr
import tempfile
from collections import OrderedDict
import os.path
import xml.sax as _sax

class RewardParameters:
    def __init__(self, pickup_reward = 0, delivery_reward = 20, late_fee = 0.02, energy_cost = 0.01,
            invalid_option_cost = 100, discount = 1):
        self.pick_reward = pickup_reward
        self.delv_reward = delivery_reward
        self.late_fee = late_fee
        self.e_cost = energy_cost
        self.invalid_opt_cost = invalid_option_cost
        self.discount = discount


class TrafficParameters:
    def __init__(self, rou_file = None, rate = 0.005, sourceIDs = [], destIDs = []):
        self.file = rou_file
        self.rate = rate
        self.sourceIDs = sourceIDs
        self.destIDs = destIDs

    @property
    def randomize(self):
        return self.file is None


class Scenario:
    """
    Class storing scenario parameters.
    """
    _Default_Agent_VType_ID = "agent"
    _Default_VTypes_File = "agent_vtype.add.xml"

    def __init__(self, net_file, stops_file, orderbook_file, vtypes_file = _Default_VTypes_File,
            fleet = {_Default_Agent_VType_ID: 1}, horizon = 3600,
            traffic_params = TrafficParameters(), reward_params = RewardParameters()):
        """
        Constructor.
        Args:
            - net_file       [str]               :
            - stops_file     [str]               :
            - orderbook_file [str]               :
            - vtypes_file    [str]               :
            - fleet          [dict of str:int]   :
            - horizon        [int]               :
            - traffic_params [TrafficParameters] :
            - rewar_params   [RewardParameters]  :
        """
        # Parse road network
        self.net = sulfr.readNet(net_file)
        self.edgeIndices = {e.getID():i for i,e in enumerate(self.net.getEdges())}

        # Parse clients' and stations' locations
        self.stops = OrderedDict( (stop.id,stop) for stop in sulfr.readStops(self.net, stops_file) )
        self.stopIndices = {stopID:i for i,stopID in enumerate(self.stops)}
        self.stations = [stop for stop in self.stops.values() if isinstance(stop, sulfr.ChargingStation)]

        # Parse clients' orders
        self.orders = sulfr.readOrderBook(self.stops, orderbook_file)

        # Store fleet composition
        self.fleet = fleet

        # Store horizon
        self.horizon = horizon

        # Traffic parameters
        self.traffic_params = traffic_params

        # Reward parameters
        self.r_params = reward_params

        # Generate command line arguments
        self._cmd_args = ["--net-file", net_file,
                "--additional-files", ','.join([stops_file, vtypes_file]),
                "--end", str(self.horizon),
                "--time-to-teleport", "-1"]
        if not self.traffic_params.randomize:
            self._cmd_args.extend( ["--route-files", self.traffic_params.file] )

    def getCommandLineArgs(self):
        """
        Get command line arguments to pass to 'sumo' or 'sumo-gui' to simulate the scenario.
        Returns:
            - [list of str] : Command line arguments
        """
        if self.traffic_params.randomize:
            ### DEBUG
            # rou_file = tempfile.NamedTemporaryFile('w', delete = False)
            # sulfr.writeRandRoutes(self.net, self.horizon, rou_file.name,
            #         self.traffic_params.rate,
            #         self.traffic_params.sourceIDs,
            #         self.traffic_params.destIDs)
            # return self._cmd_args + ["--route-files", rou_file.name]
            rou_file_name = "logs/last.rou"
            sulfr.writeRandRoutes(self.net, self.horizon, rou_file_name,
                    self.traffic_params.rate,
                    self.traffic_params.sourceIDs,
                    self.traffic_params.destIDs)
            return self._cmd_args + ["--route-files", rou_file_name]
        else:
            return self._cmd_args


class _ScenarioReader(_sax.handler.ContentHandler):
    def __init__(self, path):
        super().__init__()
        self.dirname = os.path.dirname(path)
        self.vtypes_file = "agent_vtype.add.xml"
        self.fleet = {}
        self.traffic_params = TrafficParameters()
        self.reward_params = RewardParameters()

    def startElement(self, name, attrs):
        if name == "net-file":
            self.net_file = os.path.join(self.dirname, attrs["value"])
        elif name == "stops-file":
            self.stops_file = os.path.join(self.dirname, attrs["value"])
        elif name == "orderbook-file":
            self.orderbook_file = os.path.join(self.dirname, attrs["value"])
        elif name == "vtypes-file":
            self.vtypes_file = os.path.join(self.dirname, attrs["value"])
        elif name == "type":
            self.fleet[attrs["id"]] = int(attrs["count"])
        elif name == "horizon":
            self.horizon = attrs["value"]
        elif name == "rou-file":
            self.traffic_params.rou_file = os.path.join(self.dirname, attrs["value"])
        elif name == "rate":
            self.traffic_params.rate = float(attrs["value"])
        elif name == "sourceIDS":
            self.traffic_params.sourceIDS = attrs["value"].split(',')
        elif name == "destIDS":
            self.traffic_params.destIDS = attrs["value"].split(',')
        elif name == "pick-reward":
            self.reward_params.pick_reward = float(attrs["value"])
        elif name == "delv-reward":
            self.reward_params.delv_reward = float(attrs["value"])
        elif name == "late-fee":
            self.reward_params.late_fee = float(attrs["value"])
        elif name == "e-cost":
            self.reward_params.e_cost = float(attrs["value"])
        elif name == "invalid-opt-cost":
            self.reward_params.invalid_opt_cost = float(attrs["value"])
        elif name == "discount":
            self.reward_params.discount = float(attrs["value"])

def readScenario(scn_file):
    reader = _ScenarioReader()
    _sax.parse(scn_file, reader)
    if not reader.fleet:
        reader.fleet = {"agent": 1}
    return Scenario(reader.net_file, reader.stops_file, reader.orderbook_file,
            reader.vtypes_file, reader.fleet, reader.horizon,
            reader.traffic_params, reader.reward_params)
