from xml import sax as _sax
import random

class Order:
    _State_Hidden = -1000
    _State_Pending = -100
    _State_Delivered = -10
    """
    Class to represent a package that needs to be delivered.
    """
    def __init__(self, orderID, weight, source, dest,
            discover, ready, early, late, duration):
        """
        Constructors.
        Args:
            - orderID  [str]        : Order unique identifier
            - weight   [float]      : Weight of the package in kilograms
            - source   [sulfr.Stop] : Location where the package should be picked up
            - dest     [sulfr.Stop] : Location where the package should be delivered
            - discover [int]        : Time when order is received
            - ready    [int]        : Time when package becomes available at source
            - early    [int]        : Earliest acceptable delivery time
            - late     [int]        : Latest acceptable delivery time
            - duration [int]        : Duration of load/unload
        """
        self.id = orderID
        self.weight = weight
        self.source = source
        self.dest = dest
        self.discover = discover
        self.ready = ready
        self.early = early
        self.late = late
        self.duration = duration

    def __str__(self):
        return '<order id="{}" weight="{:.3f}" srcID="{}" destID="{}" discover="{}" ready="{}" early="{}" late="{}" duration="{}" />'.format(
                        self.id, self.weight, self.source.id, self.dest.id,
                        self.discover, self.ready, self.early, self.late, self.duration)

    def reset(self):
        self.hidden = True
        self.carrier = None
        self.delivered = False

    def copy(self):
        return Order(self.id, self.weight, self.source, self.dest,
                self.discover, self.ready, self.early, self.late, self.duration)


def writeOrderBook(orders, out_file):
    with open(out_file, 'w') as obk_of:
        obk_of.write('<orderbook>\n')
        for o in orders:
            obk_of.write('\t{}\n'.format(o))
        obk_of.write('</orderbook>\n')


class _OrderBookReader(_sax.handler.ContentHandler):
    def __init__(self, stops_dict):
        super().__init__()
        self.stops = stops_dict
        self.elems = []

    def startElement(self, name, attrs):
        if name == "order":
            self.elems.append(Order(
                attrs.getValue("id"),
                float( attrs.getValue("weight") ),
                self.stops[ attrs.getValue("srcID") ],
                self.stops[ attrs.getValue("destID") ],
                int( attrs.getValue("discover") ),
                int( attrs.getValue("ready") ),
                int( attrs.getValue("early") ),
                int( attrs.getValue("late") ),
                int( attrs.getValue("duration") )
            ))


def readOrderBook(stops_dict, obk_file):
    reader = _OrderBookReader(stops_dict)
    _sax.parse(obk_file, reader)
    return reader.elems


def makeRandOrders(src_stops, dst_stops, number, horizon, dod = 0.5):
    orders = []
    for i in range(number):
        orderID = "order{}".format(i)
        weight = 10*random.random()
        disc = 0 if random.random() > dod else random.randrange(horizon // 2)
        src = random.choice(src_stops)
        dest = random.choice(dst_stops)
        ready = random.randrange(disc, min(disc+60, horizon))
        early = random.randrange(disc, horizon)
        late = random.randrange(early, horizon)
        orders.append( Order(orderID, weight, src, dest, disc, ready, early, late, 1) )
    return orders


class OrderScheduler:
    def __init__(self, simulation):
        self.sim = simulation
        self.orders = [order.copy() for order in simulation.scenario.orders]
        self.max_weight = max(order.weight for order in self.orders)
        self.max_duration = max(order.duration for order in self.orders)

    def reset(self):
        for order in self.orders:
            order.reset()
        self.discoveries = sorted(self.orders, key = lambda p:p.discover, reverse = True)
        self.pending = []

    def step(self, t = 0):
        while self.discoveries and self.discoveries[-1].discover <= t:
            order = self.discoveries.pop()
            order.hidden = False
            self.pending.append(order)

    def getState(self):
        return [order.getState() for order in self.orders]

    def getState(self):
        states = []
        for order in self.orders:
            if order.hidden:
                dyna = Order._State_Hidden
            elif order.carrier is None:
                dyna = Order._State_Pending
            elif order.delivered:
                dyna = Order._State_Delivered
            else:
                dyna = order.carrier.id

            states.append( [2*order.weight / self.max_weight - 1,
                    2*self.sim.scenario.stopIndices[order.source.id] / len(self.sim.scenario.stops) - 1,
                    2*self.sim.scenario.stopIndices[order.dest.id] / len(self.sim.scenario.stops) - 1,
                    2*order.discover / self.sim.scenario.horizon - 1,
                    2*order.ready / self.sim.scenario.horizon - 1,
                    2*order.early / self.sim.scenario.horizon - 1,
                    2*order.late / self.sim.scenario.horizon - 1,
                    2*order.duration / self.max_duration - 1] )
        return states
