from sulfr import sumolib

import subprocess
import tempfile
import random
import os


def _get_node_lbl(x, y):
    return chr(ord('A')+x) + chr(ord('0')+y)


def _make_edge(net, src, dst, length = 100, max_speed = 50 / 3.6, lane_w = 5, n_lanes = 1):
    srcID = _get_node_lbl(*src)
    dstID = _get_node_lbl(*dst)
    edgeID = "{}to{}".format(srcID, dstID)
    e = net.addEdge(edgeID, srcID, dstID, "", "", "")
    for l in range(n_lanes):
        net.addLane(e, max_speed, length, lane_w)
    return e


def _make_bidir(net, src, dst, length = 100, max_speed = 50 / 3.6, lane_w = 5, n_lanes = (1,1)):
    return ( _make_edge(net, src, dst, length, max_speed, lane_w, n_lanes[0]),
            _make_edge(net, dst, src, length, max_speed, lane_w, n_lanes[1]) )


def makeGrid(n_x, n_y, l_x = 100, l_y = 100, max_speed = 50 / 3.6, lane_w = 5):
    net = sumolib.net.Net()
    for x in range(0, n_x):
        for y in range(0, n_y):
            nodeID = _get_node_lbl(x, y)
            net.addNode(nodeID, type = "priority", coord = (x*l_x, y*l_y))
            if y > 0:
                _make_bidir(net, (x,y), (x,y-1), l_y, max_speed, lane_w)
            if x > 0:
                _make_bidir(net, (x,y), (x-1,y), l_x, max_speed, lane_w)
    return net


def makeSpider(n_c, n_a, l_a = 100, center = False, max_speed = 50, lane_w = 5):
    net = sumolib.net.Net()
    min_r = 0 if center else 1
    for th in range(0, n_a):
        for r in range(min_r, n_c+1):
            nodeID = _get_node_lbl(th, r)
            net.addNode(nodeID, type = "priority", coord = (r * l_a * cos(2*pi*th / n_a), r * l_a * sin(2*pi*th / n_a) ))
            if r > min_r:
                _make_bidir(net, (th, r), (th,r-1), l_a, max_speed, lane_w)
            if th > 0:
                _make_bidir(net, (th, r), (th-1,r), 2 * l_a * r * sin(pi / n_a), max_speed, lane_w)
    for r in range(min_r, n_c+1):
        _make_bidir(net, (0, r), (n_a-1,r), 2 * l_a * r * sin(pi / n_a), max_speed, lane_w)
    return net


def writeNet(net, out_file):
    """
    Call SUMO 'netconvert' to save road network to XML file.
    Args:
        - net      [sumolib.net.Net] : Road network
        - out_file [str]             : Path to the output file
    """
    nod_of = tempfile.NamedTemporaryFile('w')
    nod_of.write('<nodes>\n')
    for n in net.getNodes():
        nod_of.write('\t<node id="{}" x="{}" y="{}" type="{}" />\n'.format(
            n.getID(), *n.getCoord(), n.getType() ))
    nod_of.write('</nodes>')
    nod_of.flush()

    edg_of = tempfile.NamedTemporaryFile('w')
    edg_of.write('<edges>\n')
    for e in net.getEdges():
        edg_of.write('\t<edge id="{}" from="{}" to="{}" numLanes="{}" speed="{:.3f}" />\n'.format(
            e.getID(), e.getFromNode().getID(), e.getToNode().getID(), e.getLaneNumber(), e.getSpeed() ))
    edg_of.write('</edges>')
    edg_of.flush()

    subprocess.run(["netconvert", "-n", nod_of.name, "-e", edg_of.name, "-o", out_file])


def writeRandRoutes(net, horizon, out_file, rate = 0.005, source_edges = [], sink_edges = []):
    """
    Create random origin/destination pairs on a given road network,
    then generate corresponding routes through a call to SUMO 'duarouter'.
    Args:
        - net          [sumolib.net.Net]          : SUMO road network
        - horizon      [int]                      : Maximum departure time in seconds
        - out_file     [str]                      : Path to the output file
        - rate         [float]                    : Expected number of departures per source per second
        - source_edges [list of sumolib.net.Edge] : Subset of edges to sample origins from
        - sink_edges   [list of sumolib.net.Edge] : Subset of edges to sample destinations from
    """
    if not source_edges:
        source_edges = net.getEdges()
    if not sink_edges:
        sink_edges = net.getEdges()
    spawns = [random.randrange(horizon) for t in range(int(horizon * len(source_edges) * rate))]
    spawns.sort()

    trips_of = tempfile.NamedTemporaryFile('w')
    trips_of.write('<trips>\n')
    for i,t in enumerate(spawns):
        srcID = random.choice(source_edges).getID()
        snkID = srcID
        while snkID == srcID:
            snkID = random.choice(sink_edges).getID()
        tripID = "{}_{}_to_{}".format(i, srcID, snkID)
        trips_of.write('\t<trip id="{}" depart="{}" from="{}" to="{}" />\n'.format(
            tripID, t, srcID, snkID ))
    trips_of.write('</trips>')
    trips_of.flush()

    net_if = tempfile.NamedTemporaryFile('w')
    writeNet(net, net_if.name)
    subprocess.run(["duarouter", "-n", net_if.name, "-r", trips_of.name, "-o", out_file])
