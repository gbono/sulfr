import sulfr

class Policy:
    def getOption(self, agent, simulation):
        raise NotImplementedError()


import random
class RandomPolicy(Policy):
    def getOption(self, agent, simulation):
        opts = []
        for pk in simulation.getPendingOrders():
            opts.append(sulfr.PickupOption(agent, pk))
        for pk in agent.carried:
            opts.append(sulfr.DeliverOption(agent, pk))
        for chg in simulation.scenario.stations:
            opts.append(sulfr.RechargeOption(agent, chg))
        return random.choice(opts)


import math
def _dist(pt1, pt2):
    return math.hypot(pt1[1]-pt2[1], pt1[0]-pt2[0])

class NearestNeighbourPolicy(Policy):
    def getOption(self, agent, simulation):
        if agent.charge > 0.5*agent.max_charge:
            closest_pick = None
            if simulation.getPendingOrders():
                closest_pick = sulfr.PickupOption(agent, min(simulation.getPendingOrders(),
                    key = lambda pk: _dist(pk.source.coord, agent.coord)))
                pick_dist = _dist(closest_pick.order.source.coord, agent.coord)

            if agent.carried:
                closest_delv = sulfr.DeliverOption(agent, min(agent.carried,
                    key = lambda pk: _dist(pk.source.coord, agent.coord)))
                delv_dist = _dist(closest_delv.order.dest.coord, agent.coord)
                if closest_pick is not None and pick_dist < delv_dist:
                    return closest_pick
                else:
                    return closest_delv

        closest_station = sulfr.RechargeOption(agent, min(simulation.scenario.stations,
            key = lambda st: _dist(st.coord, agent.coord)))
        return closest_station
