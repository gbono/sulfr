import sulfr


class Agent:
    def __init__(self, agID, vtype_ID, simulation):
        """
        Constructor.
        Args:
            - agID       [str]                   :
            - vtype_ID   [str]                   :
            - simulation [sulfr.Simulation]      :
            - spawn      [sulfr.ChargingStation] :
        """
        self.id = agID
        self.vtype_ID = vtype_ID
        self.sim = simulation
        self.ecost = simulation.scenario.r_params.e_cost

        self.all_options = [sulfr.IdleOption(self)]
        self.all_options.extend(sulfr.RechargeOption(self, station) for station in self.sim.scenario.stations)
        self.all_options.extend(sulfr.PickupOption(self, order) for order in self.sim.order_sched.orders)
        self.all_options.extend(sulfr.DeliverOption(self, order) for order in self.sim.order_sched.orders)
        self.optionIndices = {opt.id:i for i,opt in enumerate(self.all_options)}


    def __str__(self):
        return "{} {} @({:.3f}, {:.3f}, {:.3f}) >> {:.3f}m on {}     |{}| ({:.0%})".format(
                self.id,
                "(P)" if self.parked else "   ",
                *self.pose2d,
                self.edgePos, self.edgeID,
                ('='*int(10 * self.payload / self.max_payload)).center(10),
                self.charge / self.max_charge
                )


    def reset(self):
        """
        """
        # Get relevent TraCI domains
        vtype_domain = self.sim.co.vehicletype
        self.veh_domain = self.sim.co.vehicle

        station = self.sim.scenario.stops[vtype_domain.getParameter(self.vtype_ID, "spawningStationID")]

        # Create vehicle in simulation
        self.sim.co.route.add(self.id, [station.edgeID])
        self.veh_domain.add(self.id, self.id, self.vtype_ID, departPos = str(station.startPos))
        self.veh_domain.setChargingStationStop(self.id, station.id, flags = sulfr.tc.STOP_PARKING)
        self.last_stop = station
        self.parked = False

        # Get static parameters
        self.weight_empty = float( self.veh_domain.getParameter(self.id, "device.battery.vehicleMass") )
        self.max_speed = self.veh_domain.getMaxSpeed(self.id)
        self.max_payload = float( vtype_domain.getParameter(self.vtype_ID, "maximumPayloadCapacity") )
        self.max_charge = float( self.veh_domain.getParameter(self.id, "device.battery.maximumBatteryCapacity") )

        # Initialize payload and battery state
        self.payload = 0
        self.carried = []
        self.charge = self.max_charge

        # Subscribe to state update in simulation
        self.veh_domain.subscribe( self.id,
                (sulfr.tc.VAR_ROAD_ID,
                    sulfr.tc.VAR_LANEPOSITION,
                    sulfr.tc.VAR_STOPSTATE,
                    sulfr.tc.VAR_POSITION,
                    sulfr.tc.VAR_ANGLE,
                    sulfr.tc.VAR_SPEED)
                )

        # Initialize with idle option
        self.option = sulfr.IdleOption(self)


    def step(self, t = 0):
        """
        """
        sub_res = self.veh_domain.getSubscriptionResults(self.id)

        self.parked = (sub_res[sulfr.tc.VAR_STOPSTATE] & 2) == 2

        edgeID = sub_res[sulfr.tc.VAR_ROAD_ID]
        if edgeID and not edgeID.startswith(':') and not self.parked:
            self.edgeID = edgeID

        if sub_res[sulfr.tc.VAR_LANEPOSITION] != sulfr.tc.INVALID_DOUBLE_VALUE and not self.parked:
            self.edgePos = sub_res[sulfr.tc.VAR_LANEPOSITION]

        if not self.parked:
            self.coord = sub_res[sulfr.tc.VAR_POSITION]
            self.pose2d = (*self.coord, sub_res[sulfr.tc.VAR_ANGLE])
            self.speed = sub_res[sulfr.tc.VAR_SPEED]

        # Consumed can be negative (energy recuperation when breaking)
        consumed = float( self.veh_domain.getParameter(self.id, "device.battery.energyConsumed") )
        if consumed != sulfr.tc.INVALID_DOUBLE_VALUE:
            self.charge = max(self.charge - consumed, 0)

        charged = float( self.veh_domain.getParameter(self.id, "device.battery.energyCharged") )
        if charged != sulfr.tc.INVALID_DOUBLE_VALUE:
            self.charge = min(self.charge + charged, self.max_charge)

        r = self.option.step(t)
        return r - self.ecost * consumed


    def is_near(self, stop, tol = 0):
        return self.edgeID == stop.edgeID \
                and stop.startPos - tol <= self.edgePos <= stop.endPos + tol


    def move_to(self, stop):
        """
        """
        # Return if already targeting this stop
        if self.last_stop == stop:
            print("last_stop")
            return

        # If agent is parked, resume its route
        if self.parked:
            self.veh_domain.resume(self.id)
        else:
            self.veh_domain.setStop(self.id, self.last_stop.id, duration = 0,
                    flags = sulfr.tc.STOP_PARKING | stop.flag)

        # Update targeted edge (rebuild route)
        if self.edgeID != stop.edgeID or self.edgePos <= stop.startPos:
            self.veh_domain.changeTarget(self.id, stop.edgeID)
        # Handle special case where vehicle needs to perform a U-turn
        # (SUMO rerouter does nothing because targeted edge does not change)
        else:
            e = self.sim.scenario.net.getEdge(self.edgeID)
            for oe in e.getToNode().getOutgoing():
                if oe.getToNode() == e.getFromNode():
                    break
            self.veh_domain.setRoute(self.id, [self.edgeID, oe.getID(), stop.edgeID])

        self.veh_domain.setStop(self.id, stop.id, flags = sulfr.tc.STOP_PARKING | stop.flag)
        self.last_stop = stop


    def load(self, order):
        """
        """
        if order.carrier is None \
                and self.parked \
                and self.last_stop == order.source \
                and order.weight <= self.max_payload - self.payload:
            self.carried.append(order)
            order.carrier = self
            self.payload += order.weight
            self.veh_domain.setParameter(self.id, "device.battery.vehicleMass",
                    str(self.weight_empty + self.payload))
        else:
            raise Exception("{} failed to load {}".format(self.id, order.id))


    def unload(self, order):
        """
        """
        if order.carrier == self \
                and self.parked \
                and self.last_stop == order.dest \
                and not order.delivered:
            order.delivered = True
            self.payload -= order.weight
            self.veh_domain.setParameter(self.id, "device.battery.vehicleMass",
                    str(self.weight_empty + self.payload))
        else:
            raise Exception("{} failed to unload {}".format(self.id, order.id))


    def getState(self):
        (x_min, y_min), (x_max, y_max) = self.sim.scenario.net.getBBoxXY()
        x,y,theta = self.pose2d
        return [
            2 * int(self.parked) - 1,
            2 * self.edgePos / self.sim.scenario.net.getEdge(self.edgeID).getLength() - 1,
            2 * self.sim.scenario.edgeIndices[self.edgeID] / len(self.sim.scenario.net.getEdges()) - 1,
            2 * (x - x_min) / (x_max - x_min) - 1 if x_max > x_min else 0,
            2 * (y - y_min) / (y_max - y_min) - 1 if y_max > y_min else 0,
            2 * theta / 360 - 1,
            2 * self.speed / self.max_speed - 1,
            2 * self.charge / self.max_charge - 1,
            2 * self.payload / self.max_payload - 1
            ]
