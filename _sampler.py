from multiprocessing import Pool
import sulfr

class Sampler:
    def __init__(self, scenario, policy, n_jobs = 8):
        self.n_jobs = n_jobs
        self.simulations = [sulfr.Simulation("sim{}".format(j), scenario, policy) for j in range(n_jobs)]

    def run(self):
        with Pool(self.n_jobs) as pool:
            traces = pool.map(sulfr.Simulation.run, self.simulations)

        for history in traces:
            for step in history:
                print(step.state)
