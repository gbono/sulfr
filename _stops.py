import sulfr
import xml.sax as _sax
import random
from math import floor, hypot

class _AbstractStop:
    def __init__(self, stopID, lane, startPos, endPos, name = None):
        self.id = stopID
        self.lane = lane
        self.startPos = startPos
        self.endPos = endPos
        self.name = stopID if name is None else name

        px, py = self.edge.getFromNode().getCoord()
        nx, ny = self.edge.getToNode().getCoord()
        s = 0.5*(self.startPos + self.endPos)
        d = hypot(nx - px, ny - py)
        self.coord = (px + (nx - px) * (s / d), py + (ny - py) * (s / d))

    @property
    def edge(self):
        return self.lane.getEdge()

    @property
    def edgeID(self):
        return self.edge.getID()

    def __str__(self):
        return 'id="{}" lane="{}" startPos="{}" endPos="{}" name="{}"'.format(
                self.id, self.lane.getID(), self.startPos, self.endPos, self.name )


class BusStop(_AbstractStop):
    def __init__(self, stopID, lane, startPos, endPos, name = None, lines = []):
        super().__init__(stopID, lane, startPos, endPos, name)
        self.flag = sulfr.tc.STOP_BUS_STOP
        self.lines = lines

    def __str__(self):
        return '<busStop ' + super().__str__() + ' lines="{}" />'.format(" ".join(self.lines))


class ContainerStop(_AbstractStop):
    def __init__(self, stopID, lane, startPos, endPos, name = None, lines = []):
        super().__init__(stopID, lane, startPos, endPos, name)
        self.flag = sulfr.tc.STOP_CONTAINER_STOP
        self.lines = lines

    def __str__(self):
        return '<containerStop ' + super().__str__() + ' lines="{}" />'.format(" ".join(self.lines))


class ChargingStation(_AbstractStop):
    def __init__(self, stopID, lane, startPos, endPos, name = None,
            power = 22000, efficiency = 0.95, chargeInTransit = False, chargeDelay = 0):
        super().__init__(stopID, lane, startPos, endPos, name)
        self.flag = sulfr.tc.STOP_CHARGING_STATION
        self.power = power
        self.efficiency = efficiency
        self.chargeInTransit = chargeInTransit
        self.chargeDelay = chargeDelay

    def __str__(self):
        return '<chargingStation ' + super().__str__() \
                + ' power="{}" efficiency="{}" chargeInTransit="{}" chargeDelay="{}" />'.format(
                self.power, self.efficiency, int(self.chargeInTransit), self.chargeDelay)


def writeStops(stops, out_file):
    with open(out_file, 'w') as add_of:
        add_of.write('<additional>\n')
        for stop in stops:
            add_of.write( '\t{}\n'.format(stop))
        add_of.write('</additional>\n')


class _StopsReader(_sax.handler.ContentHandler):
    def __init__(self, net):
        super().__init__()
        self.net = net
        self.elems = []

    def startElement(self, name, attrs):
        if name == "busStop":
            self.elems.append(BusStop(
                attrs.getValue("id"),
                self.net.getLane( attrs.getValue("lane") ),
                float( attrs.getValue("startPos") ),
                float( attrs.getValue("endPos") ),
                attrs.get("name", None),
                attrs.get("lines", "").split()
            ))
        elif name == "containerStop":
            self.elems.append(ContainerStop(
                attrs.getValue("id"),
                self.net.getLane( attrs.getValue("lane") ),
                float( attrs.getValue("startPos") ),
                float( attrs.getValue("endPos") ),
                attrs.get("name", None),
                attrs.get("lines", "").split()
            ))
        elif name == "chargingStation":
            self.elems.append(ChargingStation(
                attrs.getValue("id"),
                self.net.getLane( attrs.getValue("lane") ),
                float( attrs.getValue("startPos") ),
                float( attrs.getValue("endPos") ),
                attrs.get("name", None),
                float( attrs.get("power", 22000) ),
                float( attrs.get("efficiency", 0.95) ),
                bool( attrs.get("chargeInTransit", 0) ),
                float( attrs.get("chargeDelay", 0) )
            ))
        else:
            pass


def readStops(net, add_file):
    reader = _StopsReader(net)
    _sax.parse(add_file, reader)
    return reader.elems


def makeRandStops(net, count = -1, park_len = 5, intersect_margin = 5, lane_w = 5,
        charging_params = None):
    stops = []
    margin = intersect_margin + lane_w
    if count < 1:
        count = len( net.getEdges() )

    park_spots = {e.getID(): list(range( floor(e.getLength() - 2 * margin) // park_len ))
            for e in net.getEdges()}

    for j in range(count):
        park = -1
        while park < 0:
            try:
                e = random.choice(net.getEdges())
                park = random.choice(park_spots[e.getID()])
            except IndexError:
                continue

        park_spots[e.getID()].remove(park)
        if charging_params is None:
            stopID = "client{}".format(j)
            name = "Client {}".format(j)
        else:
            stopID = "station{}".format(j)
            name = "Station {}".format(j)
        lane = e.getLanes()[0]
        startPos = park * park_len + margin
        endPos = startPos + park_len
        if charging_params is None:
            stops.append( ContainerStop(stopID, lane, startPos, endPos) )
        else:
            stops.append( ChargingStation(stopID, lane, startPos, endPos, **charging_params) )

    return stops
