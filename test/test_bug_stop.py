#!/usr/bin/env python3

import sulfr

scenar = sulfr.Scenario("data/scenario/small/road.net.xml",
        "data/scenario/small/stops.add.xml",
        "data/scenario/small/orders.obk.xml",
        horizon = 1100)

simu = sulfr.Simulation("sim0", scenar, None)
ag = simu.agents[0]

for i,opt in enumerate(ag.all_options):
    print(i,opt)

simu.reset()
for t in range(scenar.horizon):
    simu.co.simulationStep()
    ag.step()
    o = input('>')
    if o:
        ag.option = ag.all_options[int(o)]
        ag.option.reset()
