#!/usr/bin/env python3
#-*-coding: utf8-*-

import sulfr
import os
import time

if __name__ == "__main__":
    DIR = "data/scenario/line"
    NET_FILE = os.path.join(DIR, "line.net.xml")
    ROU_FILE = os.path.join(DIR, "line.rou.xml")
    STP_FILE = os.path.join(DIR, "stops.add.xml")
    OBK_FILE = os.path.join(DIR, "test.obk.xml")
    HRZ = 100

    os.makedirs(DIR, exist_ok = True)
    
    net = sulfr.makeGrid(5,1)
    sulfr.writeNet(net, NET_FILE)

    sulfr.writeRandRoutes(net, HRZ, ROU_FILE, 0.05)

    stations = sulfr.makeRandStops(net, 2, charging_params = {})
    clients = sulfr.makeRandStops(net)
    sulfr.writeStops(clients + stations, STP_FILE)

    orders = sulfr.makeRandOrders(stations, clients, 10, HRZ)
    sulfr.writeOrderBook(orders, OBK_FILE)

    scenar = sulfr.Scenario(NET_FILE, STP_FILE, OBK_FILE, traffic_params = sulfr.TrafficParameters(ROU_FILE), horizon = HRZ)

    sulfr.traci.start(["sumo-gui", "-S", "-Q"] + scenar.getCommandLineArgs())
    while sulfr.traci.simulation.getMinExpectedNumber() > 0:
        sulfr.traci.simulationStep()
        time.sleep(0.03)
