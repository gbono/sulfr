#!/usr/bin/env python3
#-*-coding: utf8-*-

import sulfr
import os
import time

class PromptPolicy:
    def getOption(self, agent, simu):
        for i,opt in enumerate(agent.all_options):
            print(i, ':', opt)
        choose = -1
        while choose < 0 or choose > i:
            try:
                choose = int(input('>'))
            except ValueError:
                continue
        return agent.all_options[choose]


if __name__ == "__main__":
    DIR = "data/scenario/line"
    NET_FILE = os.path.join(DIR, "line.net.xml")
    ROU_FILE = os.path.join(DIR, "line.rou.xml")
    STP_FILE = os.path.join(DIR, "stops.add.xml")
    OBK_FILE = os.path.join(DIR, "test.obk.xml")
    HRZ = 100

    scenar = sulfr.Scenario(NET_FILE, STP_FILE, OBK_FILE, traffic_params = sulfr.TrafficParameters(ROU_FILE), horizon = HRZ)
    
    simu = sulfr.Simulation("sim0", scenar, PromptPolicy())
    
    simu.run()
