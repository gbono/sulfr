#!/usr/bin/env python3
#-*-coding: utf8-*-

import sulfr
import os
import time

if __name__ == "__main__":
    DIR = "data/scenario/line"
    NET_FILE = os.path.join(DIR, "line.net.xml")
    ROU_FILE = os.path.join(DIR, "line.rou.xml")
    STP_FILE = os.path.join(DIR, "stops.add.xml")
    OBK_FILE = os.path.join(DIR, "test.obk.xml")
    HRZ = 100

    scenar = sulfr.Scenario(NET_FILE, STP_FILE, OBK_FILE, traffic_params = sulfr.TrafficParameters(ROU_FILE), horizon = HRZ)

    simu = sulfr.Simulation("sim0", scenar, sulfr.RandomPolicy())
    trace = simu.run()
    print(trace)
