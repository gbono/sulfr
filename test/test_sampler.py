#!/usr/bin/env python3

import sulfr
import os

if __name__ == "__main__":
    DIR = "data/scenario/test"
    NET_FILE = os.path.join(DIR, "line.net.xml")
    ROU_FILE = os.path.join(DIR, "line.rou.xml")
    STP_FILE = os.path.join(DIR, "stops.add.xml")
    OBK_FILE = os.path.join(DIR, "test.obk.xml")
    HRZ = 100

    scenar = sulfr.Scenario(NET_FILE, STP_FILE, OBK_FILE, traffic_file = ROU_FILE, horizon = HRZ)

    sampler = sulfr.Sampler(scenar, sulfr.RandomPolicy())
    sampler.run()
