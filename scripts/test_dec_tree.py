#!/usr/bin/env python3

import sulfr
from decision_tree import *
import numpy as np

import random
import os
import pickle

seed = os.urandom(32)
with open("logs/last_seed", 'wb') as bf:
    bf.write(seed)
random.seed(seed)


N_EPISODES = 10000

class TreePolicy:
    def __init__(self):
        self.dec_rule = DiscreteDecisionTree(1, 10)
        self.order_indices = list(range(10))
        self.f = open("logs/policy.log", 'w')

    def getOption(self, ag, simu):
        if not self.order_indices:
            return sulfr.IdleOption(ag)
        else:
            a, p = self.dec_rule.sample_action(0, epsilon)
            self.dec_rule.stored_decision = ((0, epsilon), {}, a, p)
            order = simu.order_sched.orders[self.order_indices[a]]
            if order.carrier is None:
                print("PICK", order.id, a, p, file = self.f, flush = True)
                self.dec_rule = self.dec_rule.child_nodes[a]
                return sulfr.PickupOption(ag, order)
            else:
                print("DELV", order.id, a, p, file = self.f, flush = True)
                del self.order_indices[a]
                nxt = self.dec_rule.extend(output_size = len(self.order_indices))
                self.dec_rule.child_nodes[a] = nxt
                self.dec_rule = nxt
                return sulfr.DeliverOption(ag, order)

    def reset(self):
        print('-'*64, file = self.f)
        self.order_indices = list(range(10))
        self.dec_rule = self.dec_rule.get_root()


scenar = sulfr.Scenario("data/scenario/small/road.net.xml",
        "data/scenario/small/stops.add.xml",
        "data/scenario/small/orders.obk.xml",
        horizon = 1100)

simu = sulfr.Simulation("sim0", scenar, TreePolicy())
ag = simu.agents[0]

epsilon = 0.2
lr = 0.1

with open("logs/test_tree_history.log", 'w') as f:
    for ep in range(N_EPISODES):
        try:
            history = simu.run()
        except:
            with open("logs/last_rand_state", 'wb') as bf:
                pickle.dump(random.getstate(), bf)
            simu.policy.dec_rule.save("logs/rule_dump.pickle")
            raise

        cumul = 0
        qvals = []

        print(" It#{} lr={:3f}, epsilon={:3f} ".format(ep,lr,epsilon).center(64,'-'), file = f)
        for step in reversed(history):
            if step.options:
                print(step.timestamp, step.cumul_reward, ag.all_options[step.options[0]].id, file = f)
            else:
                print(step.timestamp, step.cumul_reward, file = f)
            cumul += step.cumul_reward
            if step.terminated[0]:
                if step.options and step.options[0] == 0:
                    continue
                qvals.append(cumul)

        qvals.pop()
        qvals.reverse()
        print(" {:3f} ".format(qvals[0]).center(64,'-'), file = f, flush = True)
        simu.policy.dec_rule.back_propagate(qvals, lr)

        if ep % 100 == 0:
            epsilon *= 0.8
            lr *= 0.99

        simu.policy.reset()
simu.policy.dec_rule.save("logs/rule_dump_final.pickle")
