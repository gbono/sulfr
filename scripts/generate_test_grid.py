#!/usr/bin/env python3

import sulfr
import os
import time

if __name__ == "__main__":
    DIR = "data/scenario/small"
    NET_F = os.path.join(DIR,"road.net.xml")
    STP_F = os.path.join(DIR,"stops.add.xml")
    OBK_F = os.path.join(DIR,"orders.obk.xml")

    GRID_DIMS = 5,5
    N_CLIENTS = 20
    N_STATIONS = 2
    N_ORDERS = 10
    DOD = 0
    HORIZON = 3600

    os.makedirs(DIR, exist_ok = True)

    net = sulfr.makeGrid(*GRID_DIMS)
    sulfr.writeNet(net, NET_F)

    stops = sulfr.makeRandStops(net, N_STATIONS+N_CLIENTS, intersect_margin = 10)

    stations = [sulfr.ChargingStation("station{}".format(i), s.lane, s.startPos, s.endPos) for i,s in enumerate(stops[:N_STATIONS])]
    clients = stops[N_STATIONS:]

    sulfr.writeStops(stations + clients, STP_F)

    orders = sulfr.makeRandOrders(stations, clients, N_ORDERS, HORIZON, dod = DOD)
    sulfr.writeOrderBook(orders, OBK_F)

    scenar = sulfr.Scenario(NET_F, STP_F, OBK_F, horizon = HORIZON)

    sulfr.traci.start(["sumo-gui", "-S", "-Q"] + scenar.getCommandLineArgs())
    for t in range(scenar.horizon):
        sulfr.traci.simulationStep()
        time.sleep(0.01)
